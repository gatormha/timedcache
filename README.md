## Timed Cache
A simple dictionary-like object that only keeps key/value pairs around as long as the specified time to live.

Install from pip:
```
pip install timedcache
```

Usage example:
```python
from timedcache import TimedCache

# create a 1 minute cache
cache = TimedCache(ttl=60)

cache['sample'] = 'data'

# cache works like a dictionary
result = cache.get('sample')

# do something that takes more than a minute
...

# but forgets data once it's time to live has expired.
# result is None now.
result = cache.get('sample')

# and this will raise a KeyError
result = cache['sample']
```
