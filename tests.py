import unittest
from timedcache import TimedCache
import time


class TestTimedCache(unittest.TestCase):
    def create_cache(self, ttl=2):
        cache = TimedCache(ttl=ttl)
        cache['key1'] = 'value1'
        cache['key2'] = 'value2'
        return cache

    def test_get(self):
        cache = self.create_cache()
        self.assertEqual(cache['key1'], 'value1')
        with self.assertRaises(KeyError):
            cache['key3']
        self.assertIsNone(cache.get('key3'))

    def test_eviction(self):
        cache = self.create_cache()
        time.sleep(3)
        with self.assertRaises(KeyError):
            cache['key1']

        with self.assertRaises(KeyError):
            del cache['key1']

    def test_delete(self):
        cache = self.create_cache()
        del cache['key2']
        with self.assertRaises(KeyError):
            cache['key2']

        time.sleep(3)
        with self.assertRaises(KeyError):
            del cache['key1']

    def test_containment(self):
        cache = self.create_cache()

        self.assertTrue('key1' in cache)
        self.assertFalse('key3' in cache)

        time.sleep(3)
        self.assertFalse('key1' in cache)

    def test_len(self):
        cache = self.create_cache()
        self.assertEqual(len(cache), 2)

        time.sleep(3)
        self.assertEqual(len(cache), 0)

    def test_iter(self):
        cache = self.create_cache()
        for key in cache:
            self.assertTrue(key in cache)

        time.sleep(3)
        for key in cache:
            self.assertTrue(False)

        cache = self.create_cache()
        keys = cache.keys()
        self.assertEqual(len(keys), 2)
        self.assertTrue('key1' in keys)
        self.assertTrue('key2' in keys)

        values = cache.values()
        self.assertEqual(len(values), 2)
        self.assertTrue('value1' in values)
        self.assertTrue('value2' in values)

        items = cache.items()
        self.assertEqual(len(items), 2)
        for item in items:
            if item[0] == 'key1':
                self.assertEquals(item[1], 'value1')
            elif item[0] == 'key2':
                self.assertEquals(item[1], 'value2')
            else:
                raise Exception()

        time.sleep(1)
        cache['key3'] = 'value3'

        time.sleep(1)
        self.assertEqual(len(cache.keys()), 1)
        self.assertEqual(len(cache.values()), 1)
        self.assertEqual(len(cache.items()), 1)


if __name__ == '__main__':
    unittest.main()
